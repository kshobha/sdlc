package com.maxim.jms.producer.model;

import org.springframework.stereotype.Component;

@Component
public class Vendor {

	private String vendorName;
	private String firstName;
	private String lastName;
	private String address;
	private String city;
	private String state;
	private String zipcode;
	private String phoneNumber;
	private String email;
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		if(null==vendorName|| vendorName.isEmpty())
		{
			vendorName="vendorName";
		}
		this.vendorName = vendorName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		if(null==firstName|| firstName.isEmpty())
		{
			vendorName="firstName";
		}
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		if(null==lastName|| lastName.isEmpty())
		{
			vendorName="lastName";
		}
		this.lastName = lastName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		if(null==address|| address.isEmpty())
		{
			vendorName="address";
		}
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		if(null==city|| city.isEmpty())
		{
			vendorName="city";
		}
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		if(null==state|| state.isEmpty())
		{
			vendorName="state";
		}
		this.state = state;
	}
	public String getZipcode() {
		return zipcode;
	}
	public void setZipcode(String zipcode) {
		if(null==zipcode|| zipcode.isEmpty())
		{
			vendorName="zipcode";
		}
		this.zipcode = zipcode;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		if(null==phoneNumber|| phoneNumber.isEmpty())
		{
			vendorName="phoneNumber";
		}
		this.phoneNumber = phoneNumber;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		if(null==email|| email.isEmpty())
		{
			vendorName="email";
		}
		this.email = email;
	}
	@Override
	public String toString() {
		return "Vendor [vendorName=" + getVendorName() + ", firstName=" + getFirstName()
				+ ", lastName=" + getLastName() + ", address=" + getAddress() + ", city="
				+ getCity() + ", state=" + getState() + ", zipcode=" + getZipcode()
				+ ", phoneNumber=" + getPhoneNumber() + ", email=" + getEmail() + "]";
	}
	
}
